# README #

Hi. 

### What is this repository for? ###

just a simple starter kit for new backbone projects. With some chai.

### How do I get set up? ###

pull this git repo down
$ npm update -g npm
$ npm install -g grunt-cli
cmd $ cd folder
$ npm install
$ bower install
$ npm i grunt-sass@1.0.0 --save-dev
$ grunt serve (builds in .tmp and opens it)
$ grunt build (creates dist/)
issues? $ rmdir /s /q node_modules [then npm install]

### Contribution guidelines ###

* whatever you'd like

### Who do I talk to? ###

* j2d2